<h1>Better</h1>
<p>Simple ToDo app</p>
<h1>About</h1>
<p>App provides simple functionality. User can add and remove items from the ToDo list. 
User can also add and remove goals (goal is a category for tasks, for example, "Get a job" is a goal and "Write resume" is a task for 
this goal)</p>
<h1>Screenshots</h1>
<div>
<img src="https://user-images.githubusercontent.com/23400308/31762206-ad9bad7c-b4e4-11e7-8390-cb71aa05c8ea.png" alt="Overview" width=250  hspace="10">
<img src="https://user-images.githubusercontent.com/23400308/31762203-ad286a1a-b4e4-11e7-987e-673ce493078b.png" alt="New goal" width=250  hspace="10">
<img src="https://user-images.githubusercontent.com/23400308/31762205-ad57e2e0-b4e4-11e7-9a95-a3cdd263236b.png" alt="New task" width=250  hspace="3">
</div>
