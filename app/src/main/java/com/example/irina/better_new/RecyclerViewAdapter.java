package com.example.irina.better_new;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import android.widget.EditText;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import static com.example.irina.better_new.Constants.EMPTY_CATEGORY;
import static com.example.irina.better_new.Constants.FULL_CATEGORY;
import static com.example.irina.better_new.Constants.NO_CATEGORY;


 class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<String> records;//list of todos
    private ArrayList<String> categories;//list of categories
     RecyclerViewAdapter(Context mContext,ArrayList<String> records,ArrayList<String> categories) {
        this.context = mContext;
        this.records = records;
        this.categories = categories;
    }

    @Override
    public int getItemViewType(int position) {
        String category = categories.get(position);
        String name = records.get(position);
        //goal without category
        if(category.equals("empty")) {
            return NO_CATEGORY;
        }
        //category without goal
        else if (name.equals(category)){
            return EMPTY_CATEGORY;
        }
        //category+goal
        else return FULL_CATEGORY;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch(viewType) {
            case NO_CATEGORY:
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.textview_layout, viewGroup, false);
                return new ViewHolder0(v);
            case EMPTY_CATEGORY:
                //category should be clickable
                View v2= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_layout, viewGroup, false);
                return new ViewHolder1(v2);
            case FULL_CATEGORY:
                View v3= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.textview_layout, viewGroup, false);
                return new ViewHolder2(v3);
            default:
                View v1 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.textview_layout, viewGroup, false);
                return new ViewHolder0(v1);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        String category = categories.get(position);
        String text = records.get(position);
        int c = holder.getItemViewType();
       switch(c) {
           case NO_CATEGORY:{
               ViewHolder0 viewHolder0 = (ViewHolder0) holder;
               viewHolder0.name.setText(text);}
               break;

           case EMPTY_CATEGORY:{
               ViewHolder1 viewHolder1 = (ViewHolder1) holder;
               viewHolder1.name.setText(text);}
               break;
           case FULL_CATEGORY:{
               ViewHolder2 viewHolder2 = (ViewHolder2) holder;
               String s = text + "\n" + category;
               //making category text small and grey
               SpannableString ss1=  new SpannableString(s);
               ss1.setSpan(new RelativeSizeSpan(0.5f), text.length(), text.length()+category.length()+1,0); // set size
               ss1.setSpan(new ForegroundColorSpan(Color.WHITE), text.length(), text.length()+category.length()+1,0); // set color
               viewHolder2.name.setText(ss1);
           }
       }
    }

    @Override
    public int getItemCount() {
        return records.size();
    }

    class ViewHolder0 extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder0(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tvLayout);
        }
    }

    class ViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;

        public ViewHolder1(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.catLayout);
            itemView.setOnClickListener(this);
        }
        //add new goal if category item was clicked
        @Override
        public void onClick(View v) {
            String text = name.getText().toString();
            final int position = records.indexOf(text);//final to make it accessible from inner anonymous class
            final EditText input = new EditText(context);
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            alert.setTitle("Новая задача");
            alert.setView(input);
            alert.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String todo = input.getText().toString();
                    records.set(position, String.valueOf(todo));
                }
            });

            alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });
            alert.show();
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {
        private TextView name;

        public ViewHolder2(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tvLayout);
        }
    }

}

