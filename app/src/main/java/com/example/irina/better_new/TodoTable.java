package com.example.irina.better_new;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Irina on 05.02.2017.
 */

public class TodoTable {
    public static final String KEY_ROWID = "_id";
    public static final String KEY_DATE = "cur_date";
    public static final String KEY_SUMMARY = "summary";
    public static final String KEY_CATEGORY= "category";
    private static final String DATABASE_TABLE = "todo3";
    private Context context;
    private SQLiteDatabase database;
    private SQLHelper dbHelper;

    public TodoTable(Context context) {
        this.context = context;
    }

    public TodoTable open() throws SQLException {
        dbHelper = new SQLHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    /**
     * создать новый элемент списка дел. если создан успешно - возвращается номер строки rowId
     * иначе -1
     */
    public long createTodo(Date date, String summary, String category) {
        ContentValues initialValues = createContentValues(date, summary, category);

        return database.insert(DATABASE_TABLE, null, initialValues);
    }

    /**
     * обновить список
     */
    public boolean updateTodo(long rowId, Date date,String summary, String category) {
        ContentValues updateValues = createContentValues(date, summary, category);

        return database.update(DATABASE_TABLE, updateValues, KEY_ROWID + "="
                + rowId, null) > 0;
    }
    public int updateDb() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c=Calendar.getInstance();
        c.add(Calendar.DATE,-1);
        Date today = c.getTime();
        String reportDate = df.format(today);

        return database.delete(DATABASE_TABLE, KEY_DATE + "<?", new String[] { reportDate });
        //String sql = "DELETE FROM todo2 WHERE date(cur_date,'unixepoch') < date('now')";
        //database.execSQL(sql);
    }
    /**
     * удаляет элемент списка
     */
    public boolean deleteTodo(long rowId) {
        return database.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    public boolean deleteTodo(String name) {
        return database.delete(DATABASE_TABLE, KEY_SUMMARY + "=?", new String[] { name }) > 0;

    }
    public boolean deleteAllTodo() {
        return database.delete(DATABASE_TABLE, null, null) > 0;

    }
    public boolean changeTodo(String name, String summary) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_SUMMARY, summary);
        return database.update(DATABASE_TABLE, cv, KEY_SUMMARY + "=?", new String[] { name }) > 0;
    }

    /**
     * возвращает курсор со всеми элементами списка дел
     *
     * @return курсор с результатами всех записей
     */
    public Cursor fetchAllTodos() {
        return database.query(DATABASE_TABLE, new String[] { KEY_ROWID,
                        KEY_SUMMARY, KEY_CATEGORY }, null, null, null,
                null, null, null);
    }

    /**
     * возвращает курсор, спозиционированный на указанной записи
     */
    public Cursor fetchTodo(long rowId) throws SQLException {
        Cursor mCursor = database.query(true, DATABASE_TABLE, new String[] {
                        KEY_ROWID,  KEY_SUMMARY, KEY_CATEGORY },
                KEY_ROWID + "=" + rowId, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public Cursor fetchTodo(String name) throws SQLException {
        Cursor mCursor = database.query(true, DATABASE_TABLE, new String[] {
                        KEY_ROWID,  KEY_SUMMARY, KEY_CATEGORY },
                KEY_SUMMARY + "=?", new String[] { name }, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
    private ContentValues createContentValues(Date date, String summary, String category) {
        ContentValues values = new ContentValues();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date_string = df.format(date);
        values.put(KEY_DATE, date_string);
        values.put(KEY_SUMMARY, summary);
        values.put(KEY_CATEGORY, category);
        return values;
    }
}
