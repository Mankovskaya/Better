package com.example.irina.better_new;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Date;

//constants for category type
import static com.example.irina.better_new.Constants.EMPTY_CATEGORY;//no goal in this category
import static com.example.irina.better_new.Constants.FULL_CATEGORY;//goal in category
import static com.example.irina.better_new.Constants.NO_CATEGORY;//goal without category

public class MainActivity extends AppCompatActivity {
    LinearLayout myLayout;
    RecyclerView myRecyclerView;
    RecyclerViewAdapter myAdapter;
    ArrayList<String> myGoals;
    ArrayList<String> myCategories;
    TodoTable dbHelper;
    Context context;
    Toolbar myToolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myLayout = (LinearLayout) findViewById(R.id.activity_main);
        myRecyclerView = (RecyclerView) findViewById(R.id.rvtodo);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        dbHelper = new TodoTable(this);
        dbHelper.open();
        myGoals = new ArrayList<>();
        myCategories = new ArrayList<>();
        context = MainActivity.this;
        loadRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_category:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Новая цель");
                final EditText input = new EditText(this);
                alert.setView(input);
                alert.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String category = input.getText().toString();
                        myGoals.add(0, String.valueOf(category));
                        myCategories.add(0, String.valueOf(category));
                        myAdapter.notifyDataSetChanged();
                    }
                });

                alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });
                alert.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
//save data in DB if onPause
    @Override
    protected void onPause()
    {
        super.onPause();
        dbHelper.deleteAllTodo();
        for (int i = 0; i < myGoals.size(); i++) {
            dbHelper.createTodo(new Date(), myGoals.get(i), myCategories.get(i));
        }
    }

    void loadRecyclerView(){
        Cursor cursor = dbHelper.fetchAllTodos();
        if(cursor.moveToFirst()){
            do{
                String name = cursor.getString(cursor.getColumnIndex(dbHelper.KEY_SUMMARY));
                String category = cursor.getString(cursor.getColumnIndex(dbHelper.KEY_CATEGORY));
                myGoals.add(name);
                myCategories.add(category);
            }while (cursor.moveToNext());
        }
        myAdapter = new RecyclerViewAdapter(this.context, myGoals, myCategories);
        myRecyclerView.setAdapter(myAdapter);
        setUpItemTouchHelper();
    }

    public void addTodo(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Новая задача");
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton("Ок", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                    String value = input.getText().toString();
                    myGoals.add(String.valueOf(value));
                    myCategories.add("empty");//no category
                    myAdapter.notifyDataSetChanged();
            }
        });

        alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }
    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT|ItemTouchHelper.LEFT) {

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                //empty category don't swipe right (user swipe left if he want to delete category)
                if (viewHolder.getItemViewType()==NO_CATEGORY || viewHolder.getItemViewType()==FULL_CATEGORY) return ItemTouchHelper.RIGHT;
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                String category = myCategories.get(swipedPosition);
                switch (swipeDir) {
                    case ItemTouchHelper.RIGHT :
                        //goal without category is deleted
                        if(viewHolder.getItemViewType()==NO_CATEGORY) {
                            myGoals.remove(swipedPosition);
                            myCategories.remove(swipedPosition);
                        }
                        else
                        //goal with category is deleted, but category is saved
                        {
                            myGoals.set(swipedPosition, category);
                        }
                        break;
                    case ItemTouchHelper.LEFT :
                        //empty category is deleted if swiped left
                        if(viewHolder.getItemViewType()==EMPTY_CATEGORY)
                        {
                            myGoals.remove(swipedPosition);
                            myCategories.remove(swipedPosition);
                        }
                        break;

                }
                myAdapter.notifyDataSetChanged();

        }

    };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(myRecyclerView);

}

}
