package com.example.irina.better_new;

 abstract class Constants {
     static final int NO_CATEGORY = 0;
     static final int EMPTY_CATEGORY = 1;
     static final int FULL_CATEGORY = 2;
}